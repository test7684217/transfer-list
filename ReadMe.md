# [Transfer List](https://transfer-list-drill.netlify.app/)

- This is a mini frontend challenge where the items are shifted from list in the left to right and vice-versa using buttons.
- Either the user can shift the items all at once or shift the selected items only.


### UI Screenshot

![alt](./public/images/Screenshot.png)
