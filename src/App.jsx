import { useState } from "react";
import "./App.css";

const initialList1 = [
  { value: "JS", checked: false },
  { value: "HTML", checked: false },
  { value: "CSS", checked: false },
  { value: "TS", checked: false },
];

const initialList2 = [
  { value: "React", checked: false },
  { value: "Angular", checked: false },
  { value: "Vue", checked: false },
  { value: "Svelte", checked: false },
];

function App() {

  const [list1, setList1] = useState(initialList1);
  const [list2, setList2] = useState(initialList2);

  const updateStateList = (e, index, list, setList) => {
    const updatedList = list.map((item, idx) =>
      idx === index ? { ...item, checked: e.target.checked } : item
    );
    setList(updatedList);
  };

  const handleShift = (sourceList, setSourceList, targetList, setTargetList) => {
    const itemsToMove = sourceList.filter((item) => item.checked);
    const updatedSourceList = sourceList.filter((item) => !item.checked);
    setSourceList(updatedSourceList);
    setTargetList([...targetList, ...itemsToMove.map((item) => ({ ...item, checked: false }))]);
  };

  const handleShiftAll = (sourceList, setSourceList, targetList, setTargetList) => {
    setTargetList([...targetList, ...sourceList.map((item) => ({ ...item, checked: false }))]);
    setSourceList([]);
  };

  const isShiftLeftDisabled = !list2.some((item) => item.checked);
  const isShiftRightDisabled = !list1.some((item) => item.checked);

  const allShiftRight = list1.length !=0;
  const allShiftLeft =  list2.length !=0;

  return (
    <div className="flex items-center flex-col">
      <div className="flex justify-center items-center h-[70px] p-4 bg-slate-100 w-full border-b-[2px] ">
        <h1 className="text-[30px] font-bold">Transfer List</h1>
      </div>
      <div className="container flex w-[80vw] border-2 border-black justify-center mt-10">
        <div className="left flex flex-col gap-4 w-[40%] p-4">
          {list1.map((item, index) => (
            <div className="flex gap-2" key={item.value}>
              <input
                type="checkbox"
                checked={item.checked}
                onChange={(e) => updateStateList(e, index, list1, setList1)}
              />
              <label>{item.value}</label>
            </div>
          ))}
        </div>
        <div className="center flex flex-col gap-4 w-[20%] border-r-[2px] border-l-[2px] border-black p-4 items-center">
          <button
            onClick={() => handleShiftAll(list2, setList2, list1, setList1)}
            className={`p-1 border-2 h-fit w-[30px] ${!allShiftLeft ? "bg-gray-200" : ""}`}
            disabled={!allShiftLeft}
          >
            {"<<"}
          </button>
          <button
            onClick={() => handleShift(list2, setList2, list1, setList1)}
            className={`p-1 border-2 h-fit w-[30px] ${isShiftLeftDisabled ? "bg-gray-200" : ""}`}
            disabled={isShiftLeftDisabled}
          >
            {"<"}
          </button>
          <button
            onClick={() => handleShift(list1, setList1, list2, setList2)}
            className={`p-1 border-2 h-fit w-[30px] ${isShiftRightDisabled ? "bg-gray-200" : ""}`}
            disabled={isShiftRightDisabled}
          >
            {">"}
          </button>
          <button
            onClick={() => handleShiftAll(list1, setList1, list2, setList2)}
            className={`p-1 border-2 h-fit w-[30px] ${!allShiftRight ? "bg-gray-200" : ""}`}
            disabled={!allShiftRight}
          >
            {">>"}
          </button>
        </div>
        <div className="right flex flex-col gap-4 w-[40%] p-4">
          {list2.map((item, index) => (
            <div className="flex gap-2" key={item.value}>
              <input
                type="checkbox"
                checked={item.checked}
                onChange={(e) => updateStateList(e, index, list2, setList2)}
              />
              <label>{item.value}</label>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default App;
